package org.stutzen.webframework.util;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

public class SWFUtil
{
	public static String stackTraceToString(Throwable e)
	{
		StringBuilder sb = new StringBuilder();
		try {
			for (StackTraceElement element : e.getStackTrace()) {
				sb.append(element.toString());
				sb.append("\n");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	public static boolean isAjax(HttpServletRequest request)
	{
		return ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) || (request.getParameter("jsonp") != null);
	}

	public static String getRedirectURL(String contextPath, String redirectURL) {
		return "redirect:" + redirectURL;
	}

	public static String getRandomKey()
	{
		String orgKey = "";
		String keyInstance = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		Random numGen = new Random();
		StringBuilder keyCollector = new StringBuilder();

		for (int i = 0; i < 30; i++) {
			int keySelector = numGen.nextInt(keyInstance.length());
			keyCollector.append(keyInstance.charAt(keySelector));
		}
		orgKey = new String(keyCollector);

		return orgKey;
	}

	public static int getRandomNumber()
	{
		int orgKey = 0;
		String keyInstance = "1234567890";
		Random numGen = new Random();
		StringBuilder keyCollector = new StringBuilder();

		for (int i = 0; i < 7; i++) {
			int keySelector = numGen.nextInt(keyInstance.length());
			keyCollector.append(keyInstance.charAt(keySelector));
		}
		orgKey = Integer.parseInt(new String(keyCollector));

		return orgKey;
	}

	public static String Nz(Object xValue1, String xValue2)
	{
		if (xValue1 == null) return xValue2;
		String tmpVal = xValue1.toString();
		if (checkNull(tmpVal)) {
			return xValue2;
		}
		return tmpVal;
	}

	public static String Inz(String xValue1) {
		if (xValue1 == null) return null;
		String tmpVal = xValue1.toString();
		if (checkNull(tmpVal)) {
			return Integer.parseInt(tmpVal) > 0 ? tmpVal : null;
		}
		return null;
	}

	public static boolean checkNull(String xParam)
	{
		xParam = xParam != null ? xParam.trim() : xParam;
		if ((xParam == null) || (xParam.length() == 0) || (xParam == "") || (xParam == "null")) {
			return true;
		}
		return false;
	}

	public static boolean isNz(Object xParam)
	{
		if (xParam == null) {
			return true;
		}
		return false;
	}

	public static String getFileExtension(String fileName)
	{
		return fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length()).toLowerCase();
	}

	public static Object IIF(boolean xCondn, Object xValueIfTrue, Object xValueIfFalse) {
		return xCondn ? xValueIfTrue : xValueIfFalse;
	}

	public static boolean IIF(boolean xCondn, boolean xValueIfTrue, boolean xValueIfFalse)
	{
		return xCondn ? xValueIfTrue : xValueIfFalse;
	}

	public static String IIF(boolean xCondn, String xValueIfTrue, String xValueIfFalse) {
		return xCondn ? xValueIfTrue : xValueIfFalse;
	}

	public static int IIF(boolean xCondn, int xValueIfTrue, int xValueIfFalse) {
		return xCondn ? xValueIfTrue : xValueIfFalse;
	}
}