package org.stutzen.webframework.constants;

public class SWFConstants
{
	public static final String LOG_SEPARATOR = "|";
	public static final String METHOD_SEPERATOR = ">>";
	public static final String EXCEPTION_TRACE_SEPERATOR = "\n";
	public static final String DEFAULT_SUCCESS_URL = "/";
	public static final String DEFAULT_LOGIN_URL = "/login";
	public static final String DEFAULT_LOGINFAILED_URL = "/login?error";
	public static final String DEFAULT_LOGOUT_URL = "/login?logout";
	public static final String API_TOKEN_HEADER = "SWF-AuthToken";
	public static final long API_TOKEN_EXPIRY = 1209600L;
	public static final String API_URL = "/api";
	public static final String ISSUER = "stutzen";
	public static final String SIGNING_KEY = "e37f4b31-0c45-11dd-bd0b-0800200c9a55";
	public static final String DEFAULT_SUCCESS_JSP = "index";
	public static final String DEFAULT_LOGIN_JSP = "login";
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static final String JDBC_USER = "root";
	public static final Integer JDBC_POOL_MINSIZE = Integer.valueOf(1);

	public static final Integer JDBC_POOL_MAXSIZE = Integer.valueOf(15);

	public static final Boolean JDBC_POOL_BREAKAFTERFAILURE = Boolean.valueOf(false);

	public static final Integer JDBC_POOL_RETRYATTEMPT = Integer.valueOf(3);

	public static final Integer JDBC_POOL_IDLETESTPERIOD = Integer.valueOf(300);

	public static final Boolean JDBC_POOL_TESTONCHECKOUT = Boolean.valueOf(true);
	
	public static final int ADMIN = 1;
	
	public static final int STAFF = 2;
	
	public static final int BELONG_TO_USER_IMG = 7;
}