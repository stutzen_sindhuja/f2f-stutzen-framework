package org.stutzen.webframework.service;

import java.util.ArrayList;

import org.apache.ibatis.exceptions.PersistenceException;

public abstract interface SWFService<T>
{
	public abstract T get(Integer paramInteger)
			throws PersistenceException;

	public abstract ArrayList<T> getAll()
			throws PersistenceException;

	public abstract T create(T paramT)
			throws PersistenceException;

	public abstract int update(T paramT)
			throws PersistenceException;

	public abstract int delete(Integer paramInteger)
			throws PersistenceException;
}