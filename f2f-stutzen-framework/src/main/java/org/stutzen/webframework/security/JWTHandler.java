package org.stutzen.webframework.security;

import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.util.Calendar;
import java.util.regex.Pattern;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JWTHandler
{
	private String ISSUER;
	private String SIGNING_KEY;

	public JWTHandler(String issuer, String key)
	{
		this.ISSUER = issuer;
		this.SIGNING_KEY = key;
	}

	public String getJWT(String username,int userid,String role)throws InvalidKeyException, SignatureException
	{
		JsonToken token = createToken(username,userid,role);
		return token.serializeAndSign();
	}

	private JsonToken createToken(String username,int userid,String role) throws InvalidKeyException
	{
		Calendar cal = Calendar.getInstance();
		HmacSHA256Signer signer = new HmacSHA256Signer(this.ISSUER, null, this.SIGNING_KEY.getBytes());
		JsonToken token = new JsonToken(signer);
		token.setAudience("SWF");
		token.setParam("typ", "SWF/api");
		token.setIssuedAt(new org.joda.time.Instant(cal.getTimeInMillis()));
		token.setExpiration(new org.joda.time.Instant(cal.getTimeInMillis() + 2592000000l));
		token.setParam("userName", username);
		token.setParam("userId", userid);
		token.setParam("role",role);
		JsonObject user = new JsonObject();
		user.addProperty("userName", username);
		user.addProperty("userId", userid);
		user.addProperty("version", "1.0.8");
		JsonObject payload = token.getPayloadAsJsonObject();
		payload.add("user", user);
		return token;
	}

	public String deserialize(String tokenString) {
		String[] pieces = splitTokenString(tokenString);
		String jwtPayloadSegment = pieces[1];
		JsonParser parser = new JsonParser();
		JsonElement payload = parser.parse(StringUtils.newStringUtf8(Base64.decodeBase64(jwtPayloadSegment.getBytes())));
		return payload.toString();
	}

	private String[] splitTokenString(String tokenString)
	{
		String[] pieces = tokenString.split(Pattern.quote("."));
		if (pieces.length != 3) {
			throw new IllegalStateException("Expected JWT to have 3 segments separated by '.', but it has " +
					pieces.length + " segments");
		}
		return pieces;
	}
}