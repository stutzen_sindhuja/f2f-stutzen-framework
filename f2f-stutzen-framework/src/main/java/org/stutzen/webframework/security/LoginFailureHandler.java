package org.stutzen.webframework.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.stutzen.webframework.common.Result;

public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler
{

	/*@Value("${security.loginfailed.url:/login?error}")
	String failureurl;*/
	public static final String LAST_USERNAME_KEY = "LAST_USERNAME";

	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException
	{
		HttpSession session = request.getSession(false);

		if ((exception instanceof DisabledException)) {
			String lastUserName = request.getParameter("username");
			if ((session != null) || (isAllowSessionCreation())) {
				request.getSession().setAttribute("LAST_USERNAME", lastUserName);
			}
		}
		else if ((session != null) && (request.getSession().getAttribute("LAST_USERNAME") != null)) {
			request.getSession().removeAttribute("LAST_USERNAME");
		}

		try {
			Result result = new Result();
			result.setSuccess(false);
			result.setMessage(exception.getLocalizedMessage());
			response.getWriter().write(result.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

/*		if (SWFUtil.Nz(request.getParameter("fromDevice"), "").equals("browser")) {
			super.setDefaultFailureUrl(this.failureurl);
			super.onAuthenticationFailure(request, response, exception);
		} else {
			try {
				Result result = new Result();
				result.setSuccess(false);
				result.setMessage(exception.getLocalizedMessage());
				response.getWriter().write(result.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	}
}