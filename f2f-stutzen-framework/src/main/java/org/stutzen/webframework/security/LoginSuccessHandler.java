package org.stutzen.webframework.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.constants.SWFConstants;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;
import org.stutzen.webframework.util.SWFUtil;

public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Value("${security.success.url:/}")
	String successurl;

	@Autowired
	private DataSource dataSource;

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)throws ServletException, IOException {
		LoggerManager.info(LogFormater.getLogMessage(getClass(),"onAuthenticationSuccess", "Enter into success handler"));
		JdbcTemplate jdbcTemplateObject = new JdbcTemplate(dataSource);
		int compId = 0;
		int staffId = 0;
		int userType = 0;
		String imgUrl = "";
		int userId = jdbcTemplateObject.queryForObject("select userid from tbl_user where username=?", Integer.class,new Object[] { authentication.getName() });
		userType = jdbcTemplateObject.queryForObject("select user_type from tbl_user where userid=?", Integer.class,new Object[] { userId });
		String name = jdbcTemplateObject.queryForObject("select first_name from tbl_user where username=?", String.class,new Object[] { authentication.getName() });
		try {
			if (userType == SWFConstants.ADMIN)
				compId = jdbcTemplateObject.queryForObject("select id from tbl_company_details where user_id=?",Integer.class, new Object[] { userId });
		} catch (EmptyResultDataAccessException e) {
		}
		try {
			if (userType == SWFConstants.STAFF) {
				staffId = jdbcTemplateObject.queryForObject("select id from tbl_staff where user_id=?",Integer.class, new Object[] { userId });
				compId = jdbcTemplateObject.queryForObject("select company_id from tbl_staff where user_id=?",Integer.class, new Object[] { staffId });
			}
		} catch (EmptyResultDataAccessException e) {
		}
		String mobile = jdbcTemplateObject.queryForObject("select mobile from tbl_user where userid=?", String.class,new Object[] { userId });
		String email = jdbcTemplateObject.queryForObject("select email from tbl_user where userid=?", String.class,new Object[] { userId });
		try {
			imgUrl = jdbcTemplateObject.queryForObject("select COALESCE(img_url, '') as img_url from tbl_image where belong_to_id=? AND belong_to=?",String.class, new Object[] { userId, SWFConstants.BELONG_TO_USER_IMG});
		} catch (EmptyResultDataAccessException e) {
		}
		Result result = new Result();
		result.setSuccess(true);
		result.setMessage("login success");
		result.addData("userName", authentication.getName());
		result.addData("mobile", mobile);
		result.addData("name", name);
		result.addData("email", email);
		result.addData("userType", userType);
		result.addData("userId", userId);
		result.addData("companyId", compId);
		result.addData("staffId", staffId);
		result.addData("imgUrl", imgUrl);
		result.addData("role", authentication.getAuthorities());

		if (SWFUtil.Nz(request.getParameter("fromDevice"), "").equals("browser")) {
			Cookie[] cookies = request.getCookies();
			if(cookies != null){
				for(Cookie cookie : cookies){
					if(cookie.getName().equalsIgnoreCase("JSESSIONID")){
						Cookie c = new Cookie("f2f-auth-key", request.getSession().getId());
						c.setPath("/");
						response.addCookie(c);
					}
				}
			}
			response.getWriter().write(result.toString());
		} else {
			List rolename = (List) authentication.getAuthorities();
			String role = "";
			if (rolename.size() > 0) {
				role = rolename.get(0).toString();
			}
			try {
				result.addData("token", new JWTHandler("stutzen", "e37f4b31-0c45-11dd-bd0b-0800200c9a55").getJWT(authentication.getName(), userId, role));
			} catch (Exception e) {
				e.printStackTrace();
			}
			response.getWriter().write(result.toString());
		}

		/* Below code is used to store user id in session [Request from Browser] */
		try {
			HttpSession session = request.getSession(true);
			session.setAttribute("userId", userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
