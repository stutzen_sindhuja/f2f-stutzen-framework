package org.stutzen.webframework.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;


public class SWFTokenSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	String apiurl;

	@Autowired
	DataSource dataSource;

	public SWFTokenSuccessHandler(String apiurl) {
		this.apiurl = "/api";
	}

	protected String determineTargetUrl(HttpServletRequest request,
			HttpServletResponse response) {
		String context = request.getContextPath();
		String fullURL = request.getRequestURI();
		String url = fullURL.substring(fullURL.indexOf(context)
				+ context.length() + this.apiurl.length());
		return url;
	}

	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		String url = determineTargetUrl(request, response);
		request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, authentication);
		System.out.println("Role"
				+ authentication.getAuthorities());
		/*try {
			HttpSession session = request.getSession(true);


			JdbcTemplate jdbcTemplateObject = new JdbcTemplate(dataSource);
			System.out.println("############User name"
					+ authentication.getName());
			int userId = jdbcTemplateObject.queryForObject(
					"select userid from tbl_user where username=?",
					Integer.class, new Object[] { authentication.getName() });
			System.out.println("############User userId" + userId);
			session.setAttribute("userId", userId);
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		LoggerManager.info(LogFormater.getLogMessage(getClass(),
				"onAuthenticationSuccess", "URL : " + url));
		request.getRequestDispatcher(url).forward(request, response);
	}

}