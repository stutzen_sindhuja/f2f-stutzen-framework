package org.stutzen.webframework.security;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;
import org.stutzen.webframework.util.SWFUtil;

public class SWFTokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter
{

	@Value("${security.api.headerkey:SWF-AuthToken}")
	String header;

	@Value("${security.api.url:/api}")
	String apiurl;

	protected SWFTokenAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher)
	{
		super(requiresAuthenticationRequestMatcher);
		setAuthenticationManager(new NoOpAuthenticationManager());
		setAuthenticationSuccessHandler(authSuccess());
	}

	@Bean
	public SWFTokenSuccessHandler authSuccess() {
		return new SWFTokenSuccessHandler(this.apiurl);
	}

	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException
	{
		String token = request.getHeader(this.header);

		AbstractAuthenticationToken userAuthenticationToken = null;
		if (!SWFUtil.checkNull(token)) {
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "attemptAuthentication", "token : " + token));
			userAuthenticationToken = authUserByToken(token);
		}
		if ((userAuthenticationToken == null) || (!userAuthenticationToken.isAuthenticated())) throw new AuthenticationServiceException(MessageFormat.format("Error | {0}", new Object[] { "Bad Token" }));
		return userAuthenticationToken;
	}

	private AbstractAuthenticationToken authUserByToken(String token)
	{
		AbstractAuthenticationToken authToken = null;
		try {
			return authToken = new JWTAuthenticationToken(token);
		} catch (Exception e) {
			LoggerManager.info(LogFormater.getLogMessage(getClass(), "authUserByToken", "Authenticate user by token error: " + e));
		}
		return authToken;
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException
	{
		  // build an Authentication object with the user's info
		super.doFilter(req, res, chain);

	}
}