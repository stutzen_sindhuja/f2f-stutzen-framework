package org.stutzen.webframework.security;

import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.HmacSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;


public class JWTAuthenticationToken extends AbstractAuthenticationToken{



	private static final long serialVersionUID = 1L;
	private Object principal;
	private Object details;
	Collection<GrantedAuthority> authorities;
	private String SIGNING_KEY = "e37f4b31-0c45-11dd-bd0b-0800200c9a55";
	@Autowired
	private DataSource dataSource;

	public JWTAuthenticationToken(String jwtToken)
	{
		super(null);
		boolean isValid = false;
		try {
				final Verifier hmacVerifier = new HmacSHA256Verifier(SIGNING_KEY.getBytes());
		        VerifierProvider hmacLocator = new VerifierProvider() {
		            @Override
		            public List<Verifier> findVerifier(String id, String key){
		                return Lists.newArrayList(hmacVerifier);
		            }
		        };
		        VerifierProviders locators = new VerifierProviders();
		        locators.setVerifierProvider(SignatureAlgorithm.HS256, hmacLocator);
		        net.oauth.jsontoken.Checker checker = new net.oauth.jsontoken.Checker(){
		            @Override
		            public void check(JsonObject payload) throws SignatureException {
		            }
		        };
		        JsonTokenParser parser = new JsonTokenParser(locators,
		                checker);
		        JsonToken jsonToken;
		        try {
		        	jsonToken = parser.verifyAndDeserialize(jwtToken);
		        } catch (SignatureException e) {
		            throw new RuntimeException(e);
		        }
		        long currTime = System.currentTimeMillis();
		        JsonObject payload = jsonToken.getPayloadAsJsonObject();
		        System.out.println("CURRENT TIME >>" + currTime);
		        System.out.println("TOKEN CREATED AT >>" + jsonToken.getIssuedAt());
		        System.out.println("TOKEN EXPIRY AT >>" + jsonToken.getExpiration());

		   LoggerManager.info(LogFormater.getLogMessage(getClass(), "JWTAuthenticationToken", "payload : " + payload));
		   if ((jsonToken.getIssuer().equals("stutzen")) && (jsonToken.getAudience().equals("SWF"))) {
			  LoggerManager.info(LogFormater.getLogMessage(getClass(), "JWTAuthenticationToken", "Issuer validation success"));
				if ((jsonToken.getIssuedAt().getMillis() < currTime) && (jsonToken.getExpiration().getMillis() > currTime)) {
					LoggerManager.info(LogFormater.getLogMessage(getClass(), "JWTAuthenticationToken", "Expiry validation success"));
					isValid = true;
					String userName = jsonToken.getParamAsPrimitive("userName").getAsString();
					System.out.println(userName);
					String userId = jsonToken.getParamAsPrimitive("userId").getAsString();
					//this.principal =  new CustomUser(userName, "protected", true, true, true, true, null,  Integer.parseInt(userId));
					this.principal= userName;
					Collection<GrantedAuthority> authrole=null;
					List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
					grantedAuthorities.add(new GrantedAuthorityImpl(jsonToken.getParamAsPrimitive("role").getAsString()));
					this.authorities=grantedAuthorities;
					super.setDetails(this.details);
				}
			}
		} catch (Exception e) {
			LoggerManager.error(LogFormater.getErrorLog(getClass(), "JWTAuthenticationToken", "Error occurred while validating token", e));
		}
		super.setAuthenticated(isValid);
	}

	public Object getCredentials()
	{
		return "";
	}

	public Object getPrincipal()
	{
		return this.principal;
	}

	public Collection<GrantedAuthority> getAuthorities()
	{
		return this.authorities;
	}

	public Object getDetails()
	{
		return super.getDetails();
	}
}