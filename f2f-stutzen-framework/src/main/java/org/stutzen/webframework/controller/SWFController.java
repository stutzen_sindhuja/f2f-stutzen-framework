package org.stutzen.webframework.controller;

import java.lang.reflect.Field;

import javax.servlet.http.HttpSession;

import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.stutzen.webframework.common.Result;
import org.stutzen.webframework.service.SWFService;
import org.stutzen.webframework.util.SWFUtil;

public class SWFController<T>
{
	SWFService<T> swfService;

	public SWFController(SWFService<T> swfService)
	{
		this.swfService = swfService;
	}
	@RequestMapping(value={"/insert"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	@ResponseBody
	public Result insert(@RequestBody T type, HttpSession session) {
		Field fl = ReflectionUtils.findField(type.getClass(), "createdby");
		if (!SWFUtil.isNz(fl)) {
			ReflectionUtils.makeAccessible(fl);
			ReflectionUtils.setField(fl, type, Integer.valueOf(Integer.parseInt(SWFUtil.Nz(session.getAttribute("userId"), "-1"))));
		}
		Object data = this.swfService.create(type);
		Result result = new Result();
		result.setSuccess(true);
		result.addData("info", data);
		result.setMessage("Data saved successfuly");
		return result;
	}
	@RequestMapping(value={"/update"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	@ResponseBody
	public Result update(@RequestBody T type, HttpSession session) {
		Field fl = ReflectionUtils.findField(type.getClass(), "lastmodifiedby");
		if (!SWFUtil.isNz(fl)) {
			ReflectionUtils.makeAccessible(fl);
			ReflectionUtils.setField(fl, type, Integer.valueOf(Integer.parseInt(SWFUtil.Nz(session.getAttribute("userId"), "-1"))));
		}
		this.swfService.update(type);
		Result result = new Result();
		result.setSuccess(true);
		result.setMessage("Data updated successfuly");
		return result;
	}
	@RequestMapping(value={"/delete/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	@ResponseBody
	public Result delete(@PathVariable Integer id) { this.swfService.delete(id);
	Result result = new Result();
	result.setSuccess(true);
	result.setMessage("Data deleted successfully");
	return result; } 
	@RequestMapping(value={"/view/{id}"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	@ResponseBody
	public Result list(@PathVariable Integer id) {
		Result result = new Result();
		result.addData("info", this.swfService.get(id));
		result.setSuccess(true);
		return result;
	}
	@RequestMapping(value={"/list"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	@ResponseBody
	public Result list() { Result result = new Result();
	result.addData("list", this.swfService.getAll());
	result.setSuccess(true);
	return result;
	}
}