package org.stutzen.webframework.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController
{

	@Value("${security.success.jsp:index}")
	String successJsp;

	@RequestMapping({"${security.success.url:/}"})
	public String root(HttpSession session)
	{
		return this.successJsp;
	}
}