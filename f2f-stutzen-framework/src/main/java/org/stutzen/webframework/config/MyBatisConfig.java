package org.stutzen.webframework.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.stutzen.webframework.exception.SWFException;

@Configuration
@EnableTransactionManagement
public class MyBatisConfig
{
	@Bean
	public MapperScannerConfigurer mapper(Environment env)
			throws SWFException
	{
		MapperScannerConfigurer mapper = new MapperScannerConfigurer();
		mapper.setBasePackage(env.getProperty("basePackage") + ".mapper");
		return mapper;
	}

	@Bean
	public SqlSessionFactoryBean sqlSessionFactoryBean(Environment env, DataSource dataSource, ApplicationContext applicationContext) throws IOException, SWFException
	{
		SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
		factoryBean.setDataSource(dataSource);
		factoryBean.setTypeAliasesPackage(env.getProperty("basePackage") + ".entity");
		return factoryBean;
	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
		return new SqlSessionTemplate(sqlSessionFactory);
	}
}