package org.stutzen.webframework.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;
import org.stutzen.webframework.logger.LogFormater;
import org.stutzen.webframework.logger.LoggerManager;

@Aspect
@Configuration
public class LoggingAspect
{
	@Around("execution(* org.stutzen..*.*(..))")
	public Object logTimeMethod(ProceedingJoinPoint joinPoint)
			throws Throwable
	{
		System.out.println("Logging...");
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		Object retVal = joinPoint.proceed();

		stopWatch.stop();

		StringBuffer logMessage = new StringBuffer();
		logMessage.append("Args(");

		Object[] args = joinPoint.getArgs();
		for (int i = 0; i < args.length; i++) {
			logMessage.append(args[i]).append(",");
		}
		if (args.length > 0) {
			logMessage.deleteCharAt(logMessage.length() - 1);
		}

		logMessage.append(")");
		logMessage.append(" execution time: ");
		logMessage.append(stopWatch.getTotalTimeMillis());
		logMessage.append(" ms");

		LoggerManager.debug(LogFormater.getLogMessage(joinPoint.getTarget().getClass(), joinPoint.getSignature().getName(), logMessage.toString()));
		return retVal;
	}

	@AfterThrowing(pointcut="execution(* org.stutzen..*.*(..))", throwing="error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		StringBuffer logMessage = new StringBuffer();
		logMessage.append("Args(");

		Object[] args = joinPoint.getArgs();
		for (int i = 0; i < args.length; i++) {
			logMessage.append(args[i]).append(",");
		}
		if (args.length > 0) {
			logMessage.deleteCharAt(logMessage.length() - 1);
		}

		logMessage.append(")");
		logMessage.append(" Exception : " + error.getMessage());
		logMessage.append(error.getMessage());
		LoggerManager.error(LogFormater.getErrorLog(joinPoint.getTarget().getClass(), joinPoint.getSignature().getName(), logMessage.toString(), error));
	}
}