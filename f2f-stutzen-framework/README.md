**Stutzen Web Framework**
-------
Development ready spring web application framework. 

[Usage](https://bitbucket.org/stutzen/stutzen-web-framework/wiki/Usage)

[SWF](https://bitbucket.org/stutzen/stutzen-web-framework/wiki/SWF)

[Table](https://bitbucket.org/stutzen/stutzen-web-framework/wiki/Tables)

[Authentication](https://bitbucket.org/stutzen/stutzen-web-framework/wiki/Auth)

[Quick Start](https://bitbucket.org/stutzen/stutzen-web-framework/wiki/Quick)